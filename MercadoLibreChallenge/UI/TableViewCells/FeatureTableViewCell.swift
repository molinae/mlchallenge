//
//  FeatureTableViewCell.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 17/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import UIKit

class FeatureTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueNameLabel: UILabel!
    
    var attribute: Attribute? {
        didSet {
            nameLabel.text = attribute?.name
            valueNameLabel.text = attribute?.valueName
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
