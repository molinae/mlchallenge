//
//  ProductTableViewCell.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 16/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import UIKit
import SDWebImage

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var shipmentLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    var product: Product? {
        didSet {
            guard let product = product else {
                return
            }
            
            titleLabel.text = product.title
            shipmentLabel.isHidden = !product.shipping.freeShipping
            priceLabel.text = "\(product.price) \(product.currencyID)"
            let thumbnailUrl = URL(string: product.thumbnail)
            thumbnailImageView.sd_setImage(with: thumbnailUrl, placeholderImage: UIImage(named: "Product"))
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
