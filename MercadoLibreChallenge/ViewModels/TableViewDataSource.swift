//
//  ProductsTableViewDataSource.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 16/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation
import UIKit

class TableViewDataSource<CELL : UITableViewCell,T> : NSObject, UITableViewDataSource {
    
    private var items : [T]!
    var configureCell : (CELL, T) -> () = {_,_ in }
    var emptyMessage: String!
    
    init(emptyMessage: String) {
        self.emptyMessage = emptyMessage
        items = []
    }
    
    init(items : [T], emptyMessage: String, configureCell : @escaping (CELL, T) -> ()) {
        self.items =  items
        self.emptyMessage = emptyMessage
        self.configureCell = configureCell
    }
    
    func item(for row: Int) -> T {
        return items[row]
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let itemsCount = items.count
        if itemsCount == 0 {
            tableView.setEmptyMessage(emptyMessage)
        } else {
            tableView.restore()
        }
        
        return itemsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL.identifier, for: indexPath) as! CELL
        
        let item = self.items[indexPath.row]
        self.configureCell(cell, item)
        return cell
    }
    
}
