//
//  ProductsViewModel.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 16/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

class ProductsViewModel : NSObject {
    
    private var apiService : APIService!
    private(set) var products : [Product]! {
        didSet {
            self.bindProductsViewModelToController()
        }
    }
    
    var bindShowActivityIndicator: ((Bool) -> Void) = {_ in }
    var bindProductsViewModelToController : (() -> ()) = {}
    var bindShowAlertDialog: ((_ title: String, _ message: String) -> Void) = {_,_  in }
    
    override init() {
        super.init()
        self.apiService = APIService.instance
    }
    
    func getProducts(withQuery query: String) {
        bindShowActivityIndicator(true)
        apiService.getProducts(withQuery: query)
            .done {
                self.products = $0
            }
            .catch { error in
                debugPrint(error)
                self.products = []
                self.bindShowAlertDialog(AppUtility.infoForKey(AppUtility.PropertyKey.bundleName), error.localizedDescription)
            }
            .finally {
                self.bindShowActivityIndicator(false)
            }
    }
}



