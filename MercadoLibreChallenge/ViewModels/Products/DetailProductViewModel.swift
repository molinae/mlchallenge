//
//  DetailProductViewModel.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 25/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

class DetailProductViewModel : NSObject {
    
    private var apiService : APIService!
    private(set) var product : Product!
    
    var bindActivityIndicator: ((Bool) -> Void) = {_ in }
    var bindShowAlertDialog: ((_ title: String, _ message: String) -> Void) = {_,_  in }
    
    init(product: Product) {
        super.init()
        self.apiService = APIService.instance
        self.product = product
    }
    
    func buyProduct() {
        bindActivityIndicator(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.bindShowAlertDialog("Compra", "Próximamente...")
            self.bindActivityIndicator(false)
        }
        
    }
}

