//
//  ViewController.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 15/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var productsTableView: UITableView!
    
    private var productsViewModel : ProductsViewModel!
    private var resultSearchController: UISearchController!
    
    private var dataSource: TableViewDataSource<ProductTableViewCell, Product>?
    
    var searchController : UISearchController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        callToViewModelForUIUpdate()
        setupUI()
    }
    
    func setupUI() {
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchBar.delegate = self
            controller.searchBar.sizeToFit()
            controller.searchBar.placeholder = "Buscar Producto"

            self.productsTableView.tableHeaderView = controller.searchBar

            return controller
        })()
        self.hideKeyboardWhenTappedAround()
    }

    func callToViewModelForUIUpdate() {
        self.productsViewModel =  ProductsViewModel()
        self.productsViewModel.bindProductsViewModelToController = {
            self.updateDataSource()
        }
        
        self.productsViewModel.bindShowActivityIndicator = { show in
            if show {
                self.showActivityIndicator()
                self.updateDataSource(message: "")
            } else {
                self.hideActivityIndicator()
            }
            
        }
        
        self.productsViewModel.bindShowAlertDialog = { (title, message) in
            self.updateDataSource(message: message)
        }
    }
    
    func updateDataSource(message: String) {
        self.dataSource = TableViewDataSource(emptyMessage: message)
           
        DispatchQueue.main.async {
            self.productsTableView.dataSource = self.dataSource
            self.productsTableView.reloadData()
        }
    }
       
    func updateDataSource(){
        let items = self.productsViewModel.products
        self.dataSource = TableViewDataSource(items: items ?? [], emptyMessage: "No se encontraron productos", configureCell: { (cell, evm) in
            let productTableViewCell = cell as ProductTableViewCell
            productTableViewCell.product = evm
        })
           
        DispatchQueue.main.async {
            self.productsTableView.dataSource = self.dataSource
            self.productsTableView.reloadData()
        }
    }
    
    // MARK: - Navigation

      // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = productsTableView.indexPathForSelectedRow {
            if let detailProductViewController = segue.destination as? DetailProductViewController {
                let product = dataSource?.item(for: indexPath.row)
                detailProductViewController.product = product!
            }
            productsTableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    
    // MARK: - UISearchBarDelegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchText = searchBar.text!
        productsViewModel.getProducts(withQuery: searchText)
    }

}

