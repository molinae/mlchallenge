//
//  DetailProductViewController.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 16/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import UIKit
import UICircularProgressRing

class DetailProductViewController: UIViewController {
    
    var product: Product!
    
    private var detailProductViewModel : DetailProductViewModel!

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var shipmentLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var featuresTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sellerLabel: UILabel!
    @IBOutlet weak var sellerProgress: UICircularProgressRing!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var powerSellerStatusLabel: UILabel!
    @IBOutlet weak var eshopLogoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        callToViewModelForUIUpdate()
    }
    
    func setupUI() {
        featuresTableView.dataSource = self
        
        title = product.title
        titleLabel.text = product.title
        
        conditionLabel.text = "\(product.condition.localizedDescription) | \(product.soldQuantity) Vendidos"
        shipmentLabel.isHidden = !product.shipping.freeShipping
        priceLabel.text = "\(product.price) \(product.currencyID)"
        let thumbnailUrl = URL(string: product.thumbnail)
        thumbnailImageView.sd_setImage(with: thumbnailUrl, placeholderImage: #imageLiteral(resourceName: "Product"))
        
        let seller = product.seller
        
        sellerLabel.text = seller.eshop?.nickName
        if let levelID = seller.sellerReputation.levelID {
            sellerProgress.value = CGFloat(levelID.progress)
            let color = UIColor.from(levelId: levelID)
            sellerProgress.innerRingColor = color
            progressLabel.text = "\(levelID.progress)º"
            
        } else {
            sellerProgress.value = CGFloat(0.0)
        }
        
        powerSellerStatusLabel.text = seller.sellerReputation.powerSellerStatus?.rawValue.capitalized
        
        if let eshopLogo = seller.eshop?.eshopLogoURL, let eshopLogoUrl = URL(string: eshopLogo) {
            eshopLogoImageView.sd_setImage(with: eshopLogoUrl, placeholderImage: #imageLiteral(resourceName: "LogoMLPortrait"))
        }
    }
    
    func callToViewModelForUIUpdate() {
        self.detailProductViewModel =  DetailProductViewModel(product: product)
   
        self.detailProductViewModel.bindActivityIndicator = { show in
            if show {
                self.showActivityIndicator()
            } else {
                self.hideActivityIndicator()
            }
            
        }
        
        self.detailProductViewModel.bindShowAlertDialog = { (title, message) in
            self.presentAlertController(title: title, message: message)
        }
    }

    //MARK: - Handle UI Responder touches
    @IBAction func touchBut(_ sender: Any) {
        detailProductViewModel.buyProduct()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailProductViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return product.attributes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeatureTableViewCell.identifier, for: indexPath) as! FeatureTableViewCell
        let item = self.product.attributes[indexPath.row]
        cell.attribute = item
        return cell
    }
    
    
}
