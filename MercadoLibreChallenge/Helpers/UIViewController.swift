//
//  UIViewController.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 17/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation
import UIKit

public extension UIViewController {
    
    /*
        Este método se llama cuando se necesita mostrar un mensaje por pantalla
    */
    
    func presentAlertController(title: String?, message: String?, preferredStyle: UIAlertController.Style = UIAlertController.Style.alert, buttonTitle: String? = "Aceptar", withCancelButton: Bool? = false, handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        if withCancelButton ?? false {
            alertController.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        }
        let alertActionOk = UIAlertAction(title: buttonTitle, style: .default) { (alertAction) -> Void in
            handler?(alertAction)
        }
        alertController.addAction(alertActionOk)
        
        self.present(alertController, animated: true, completion: nil)
    }

    func showMessageErrorConection() {
        presentAlertController(title: "Error de conexión", message: "Verifique su conexión a Internet.")
    }

    func showMessageErrorServicio() {
        presentAlertController(title: "", message: "Error al consultar el servicio.")
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /*
        Este método se llama cuando se necesita mostrar un progress por pantalla
    */
    
    func showActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.backgroundColor = UIColor.mlYellowA700
        activityIndicator.layer.cornerRadius = 6
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.large
        activityIndicator.startAnimating()
        activityIndicator.tag = 100
        
        for subview in view.subviews {
            if subview.tag == 100 {
                print("already added")
                return
            }
        }
        
        view.addSubview(activityIndicator)
    }
    
    /*
        Este método se llama cuando se necesita ocultar un progress por pantalla
    */
    
    func hideActivityIndicator() {
        let activityIndicator = view.viewWithTag(100) as? UIActivityIndicatorView
        activityIndicator?.stopAnimating()
        activityIndicator?.removeFromSuperview()
    }
}

