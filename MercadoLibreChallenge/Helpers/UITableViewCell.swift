//
//  UITableViewCell.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 18/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
}
