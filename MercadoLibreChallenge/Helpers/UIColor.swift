//
//  UIColor.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 17/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init?(hex: String) {
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    let r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    let g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    let b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    let a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
    
    static let mlYellowA700 = UIColor(hex: "#FFD600")
    static let mlred500 = UIColor(hex: "#F44336")
    static let mlOrange500 = UIColor(hex: "#FF9800")
    static let mlYellow500 = UIColor(hex: "#FFEB3B")
    static let mlLightGreen500 = UIColor(hex: "#8BC34A")
    static let mlGreen500 = UIColor(hex: "#4CAF50")
    
    static func from(levelId: LevelID) -> UIColor {
        switch levelId {
        case .the1Red: return UIColor.red
        case .the2Orange: return UIColor.orange
        case .the3Yellow: return UIColor.yellow
        case .the4LightGreen: return UIColor(red: 139, green: 195, blue: 74, alpha: 1)
        case .the5Green: return UIColor.green
        }
    }
}
