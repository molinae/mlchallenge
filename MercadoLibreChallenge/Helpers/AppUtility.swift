//
//  AppUtility.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 17/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

/*
  AppUtility es una utilidad que me permite obtener propiedades principales del proyecto
 
*/

struct AppUtility {
    
    enum PropertyKey: String {
        case apiBaseUrlEndPoint = "ApiBaseUrlEndpoint"
        case accessGroup = "AccessGroup"
        case bundleName = "CFBundleName"
        case versionName = "CFBundleShortVersionString"
        case bundleVersionString = "CFBundleVersion"
    }
    
    static func infoForKey(_ key: PropertyKey) -> String {
        guard let info = (Bundle.main.infoDictionary?[key.rawValue] as? String)?
            .replacingOccurrences(of: "\\", with: "") else {
                fatalError("🤬 Property \(key.rawValue) not exist")
        }
        
        return info
    }
    
     /* Permite obtener la version del sistema operativo */
    static var osVersion: String {
        let systemVersion = UIDevice.current.systemVersion
        return systemVersion
    }
    
    /* Permite obtener un identificador único por iphone o ipad */
    static var uuidDevice: String? {
        if let vendor = UIDevice.current.identifierForVendor {
            debugPrint(vendor)
            return vendor.uuidString
        }
        return nil
    }
    
    static var bundleVersion: Int {
        let bundleVersion = infoForKey(.bundleVersionString)
        
        return Int(bundleVersion) ?? 0
    }
}
