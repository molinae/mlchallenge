//
//  ApiService+Sites.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 24/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Alamofire
import AlamofireSwiftyJSON
import PromiseKit
import SwiftyJSON

extension APIService {
    
    /* obtiene productos según una consulta */
    func getProducts(withQuery query: String) -> Promise<([Product])> {
        return manager
            .request(baseUrl + "/sites/MLA/search",
                     method: .get,
                     parameters: ["q": query],
                     encoding: URLEncoding.queryString)
            .validate()
            .responseSwiftyJSON()
            .map { response -> ([Product]) in
                let resultsData = try! response.json["results"].rawData()
                
                let products = try? JSONDecoder().decode([Product].self, from: resultsData)
                
                return(products ?? [])
        }
    }
    
}

