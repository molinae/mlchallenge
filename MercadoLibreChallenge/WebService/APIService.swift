//
//  APIService.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 16/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Alamofire

class APIService : NSObject {
    
    internal let baseUrl = AppUtility.infoForKey(.apiBaseUrlEndPoint)
    
    static let instance = APIService()
    
    internal let manager: SessionManager
       
    init(manager: SessionManager = SessionManager.default) {
        self.manager = manager
    }
    
    /*var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }*/
    
}
