//
//  AlamofireDataRequest.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 17/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//
import Foundation
import Alamofire
import AlamofireSwiftyJSON
import PromiseKit
import SwiftyJSON

extension Alamofire.DataRequest {
    
    /// Adds a handler to be called once the request has finished. Provides access to the detailed response object.
    ///    request.responseJSON(with: .response).then { json, response in }
    @discardableResult
    public func responseSwiftyJSON( queue: DispatchQueue? = nil, options: JSONSerialization.ReadingOptions = .allowFragments) -> Promise<AFJSONResponse> {
        return Promise { seal in
            responseSwiftyJSON(
                queue: queue,
                options: options,
                completionHandler: { response in
                    switch response.result {
                    case .success(let value):
                        self.printRequest(response.request, json: value)
                        seal.fulfill(AFJSONResponse(json: value, response: response))
                    case .failure(let error):
                        let json = response.data.flatMap{ try? JSON(data: $0) }
                        debugPrint(error.localizedDescription)
                        seal.reject(AFJSONError(error: error, json: json, response: response))
                    }
                }
            )
        }
    }
    
    private func printRequest(_ request: URLRequest?, json: JSON) {
        if let request = request, let method = request.httpMethod {
            debugPrint(">>> \(method) \(request)")
            debugPrint("------------------------------------")
            debugPrint(json)
        }
    }
}

public struct AFJSONResponse {
    let json: JSON
    let response: Alamofire.DataResponse<JSON>
}

public struct AFJSONError: Error {
    let error: Error
    let json: JSON?
    let response: Alamofire.DataResponse<JSON>
}
