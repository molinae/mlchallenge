//
//  Ratings.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 25/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - Ratings
struct Ratings: Decodable {
    let negative, positive, neutral: Double
}
