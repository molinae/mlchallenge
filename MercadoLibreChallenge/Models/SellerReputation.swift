//
//  SellerReputation.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 25/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - SellerReputation
struct SellerReputation: Decodable {
    let transactions: Transactions
    let powerSellerStatus: PowerSellerStatus?
    let levelID: LevelID?
    let protectionEndDate, realLevel: String?

    enum CodingKeys: String, CodingKey {
        case transactions
        case powerSellerStatus = "power_seller_status"
        case levelID = "level_id"
        case protectionEndDate = "protection_end_date"
        case realLevel = "real_level"
    }
}

