//
//  Eshop.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 15/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - Eshop
struct Eshop: Decodable {
    let nickName: String
    let eshopID: Int
    let eshopLogoURL: String
    let eshopStatusID, seller, eshopExperience: Int

    enum CodingKeys: String, CodingKey {
        case nickName = "nick_name"
        case eshopID = "eshop_id"
        case eshopLogoURL = "eshop_logo_url"
        case eshopStatusID = "eshop_status_id"
        case seller
        case eshopExperience = "eshop_experience"
    }
}
