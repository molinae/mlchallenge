//
//  Seller.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 15/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - Seller
struct Seller: Decodable {
    let id: Int
    let permalink: String
    let registrationDate: String
    let eshop: Eshop?
    let carDealer: Bool
    let sellerReputation: SellerReputation

    enum CodingKeys: String, CodingKey {
        case id, permalink
        case registrationDate = "registration_date"
        case carDealer = "car_dealer"
        case eshop
        case sellerReputation = "seller_reputation"
    }
}
