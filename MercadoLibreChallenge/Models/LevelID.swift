//
//  LevelID.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 25/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - LevelID
enum LevelID: String, Decodable {
    case the1Red = "1_red"
    case the2Orange = "2_orange"
    case the3Yellow = "3_yellow"
    case the4LightGreen = "4_light_green"
    case the5Green = "5_green"
    
    var progress: Int {
        switch self {
        case .the1Red: return 1
        case .the2Orange: return 2
        case .the3Yellow: return 3
        case .the4LightGreen: return 4
        case .the5Green: return 5
        }
    }
}
