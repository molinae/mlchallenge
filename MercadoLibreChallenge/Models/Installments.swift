//
//  Installments.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 15/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - Installments
struct Installments: Decodable {
    let quantity: Int
    let amount, rate: Double
    let currencyID: String

    enum CodingKeys: String, CodingKey {
        case quantity, amount, rate
        case currencyID = "currency_id"
    }
}
