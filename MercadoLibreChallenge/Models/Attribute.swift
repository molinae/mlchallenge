//
//  Attribute.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 15/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - Attribute
struct Attribute: Decodable {
    let id: String
    let valueID: String?
    let attributeGroupName: String
    let source: Int
    let name: String
    let valueName: String

    enum CodingKeys: String, CodingKey {
        case id
        case valueID = "value_id"
        case attributeGroupName = "attribute_group_name"
        case source, name
        case valueName = "value_name"
    }
}
