//
//  Shipping.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 15/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - Shipping
struct Shipping: Decodable {
    let freeShipping: Bool
    let mode: String

    enum CodingKeys: String, CodingKey {
        case freeShipping = "free_shipping"
        case mode
    }
}
