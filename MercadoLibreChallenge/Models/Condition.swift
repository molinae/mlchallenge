//
//  Condition.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 16/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - Condition
enum Condition: String, Decodable {
    case new = "new"
    case used = "used"
    
    var localizedDescription: String {
        switch self {
        case .new: return "Nuevo"
        case .used: return "Usado"
        }
    }
}
