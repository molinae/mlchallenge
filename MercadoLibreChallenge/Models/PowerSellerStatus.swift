//
//  PowerSellerStatus.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 25/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - PowerSellerStatus
enum PowerSellerStatus: String, Decodable {
    case gold = "gold"
    case platinum = "platinum"
    case silver = "silver"
}
