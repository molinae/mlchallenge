//
//  Address.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 16/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - Address
struct Address: Decodable {
    let stateID: String
    let stateName: String
    let cityID: String?
    let cityName: String

    enum CodingKeys: String, CodingKey {
        case stateID = "state_id"
        case stateName = "state_name"
        case cityID = "city_id"
        case cityName = "city_name"
    }
}
