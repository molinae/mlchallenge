//
//  Product.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 15/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - Product
struct Product: Decodable {
    
    let id: String
    let siteID: String
    let title: String
    let seller: Seller
    let price: Double
    let currencyID: String
    let availableQuantity, soldQuantity: Int
    let stopTime: String
    let condition: Condition
    let permalink: String
    let thumbnail: String
    let thumbnailID: String
    let acceptsMercadopago: Bool
    let installments: Installments
    let address: Address
    let shipping: Shipping
    let attributes: [Attribute]
    let originalPrice: Double?
    let domainID: String
    let tags: [String]
    let catalogListing: Bool?
    let useThumbnailID: Bool
    let orderBackend: Int

    enum CodingKeys: String, CodingKey {
        case id
        case siteID = "site_id"
        case title, seller, price
        case currencyID = "currency_id"
        case availableQuantity = "available_quantity"
        case soldQuantity = "sold_quantity"
        case stopTime = "stop_time"
        case condition, permalink, thumbnail
        case thumbnailID = "thumbnail_id"
        case acceptsMercadopago = "accepts_mercadopago"
        case installments, address, shipping
        case attributes
        case originalPrice = "original_price"
        case domainID = "domain_id"
        case tags
        case catalogListing = "catalog_listing"
        case useThumbnailID = "use_thumbnail_id"
        case orderBackend = "order_backend"
    }
}
