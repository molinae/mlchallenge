//
//  Transactions.swift
//  MercadoLibreChallenge
//
//  Created by Emanuel Molina on 25/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import Foundation

// MARK: - Transactions
struct Transactions: Decodable {
    let total, canceled: Int
    let ratings: Ratings
    let completed: Int
}
