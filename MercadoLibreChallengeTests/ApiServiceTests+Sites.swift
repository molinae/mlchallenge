//
//  ApiServiceTests+Sites.swift
//  MercadoLibreChallengeTests
//
//  Created by Emanuel Molina on 24/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import XCTest
import Foundation
import Alamofire

class ApiServicesSitesTest: ApiServiceTests {
    
    func testGetProductsSuccess() {
        let expectation = XCTestExpectation(description: "Get Products Success")
        
        let mockJSONData = readJSON(resourceUrl: "GetProductsSuccess")
        
        MockURLProtocol.responseWithStatus(code: 200, data: mockJSONData)
        
        sut.getProducts(withQuery: "")
            .done { products in
                XCTAssertNotNil(products)
                let product = products.first
                XCTAssertEqual(product?.id, "MLA909372335")
                XCTAssertEqual(product?.title, "Moto G6 Dual Sim 64 Gb Negro 4 Gb Ram")
                
                let seller = product?.seller
                XCTAssertNotNil(seller)
                XCTAssertEqual(seller?.id, 233230004)
                
                let eshop = seller?.eshop
                XCTAssertNotNil(eshop)
                XCTAssertEqual(eshop?.nickName, "MISIONLIVE")
                
                let sellerReputation = seller?.sellerReputation
                XCTAssertNotNil(sellerReputation)
                
                let levelId = sellerReputation?.levelID
                XCTAssertNotNil(levelId)
                XCTAssertEqual(levelId, LevelID.the5Green)
                XCTAssertEqual(levelId?.progress, 5)
                
                expectation.fulfill()
                
            }
            .catch { err in
                XCTAssertThrowsError(err)
                
                expectation.fulfill()
        }
        
        // Wait until the expectation is fulfilled, with a timeout of 120 seconds.
        wait(for: [expectation])
    }
    
}
