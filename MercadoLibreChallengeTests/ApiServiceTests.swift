//
//  ApiServiceTests.swift
//  MercadoLibreChallengeTests
//
//  Created by Emanuel Molina on 24/05/2021.
//  Copyright © 2021 Emanuel Molina. All rights reserved.
//

import XCTest
import Foundation
import Alamofire

class ApiServiceTests: XCTestCase {
    
    fileprivate(set) var sut: APIService!
    
    fileprivate(set) var testBundle: Bundle!

    func wait(for expectations: [XCTestExpectation]) {
        self.wait(for: expectations, timeout: 10)
    }

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let manager: SessionManager = {
            let configuration: URLSessionConfiguration = {
                let configuration = URLSessionConfiguration.default
                configuration.protocolClasses = [MockURLProtocol.self]
                return configuration
            }()
            
            return SessionManager(configuration: configuration)
        }()
        
        sut = APIService(manager: manager)
        
        testBundle = Bundle(for: type(of: self))
    }
    
    func readJSON(resourceUrl: String) -> Data? {
        let t = type(of: self)
        let bundle = Bundle(for: t.self)
        return bundle.data(fromJsonUrl: resourceUrl)
    }
    
    func testReadJson() {
        let url = readJSON(resourceUrl: "GetProductsSuccess")
        XCTAssertNotNil(url)
    }
}
